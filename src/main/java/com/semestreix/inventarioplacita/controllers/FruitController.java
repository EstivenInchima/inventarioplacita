package com.semestreix.inventarioplacita.controllers;


import com.semestreix.inventarioplacita.commons.Constants;
import com.semestreix.inventarioplacita.controllers.utils.HeaderUtil;
import com.semestreix.inventarioplacita.domain.Fruit;
import com.semestreix.inventarioplacita.domain.dto.FruitDTO;
import com.semestreix.inventarioplacita.service.FruitService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(Constants.NAME_APP + Constants.VERSION)
public class FruitController {

    //METODO DE RESTFULL - que utiliza el metodo http get

    @GetMapping("/hello-world")
    public ResponseEntity<String> getHelloWorld() {
        return ResponseEntity.ok()
                .headers(HeaderUtil.createAlert("ok", "ok"))
                .body("funciona");
    }

    private final FruitService fruitService;


    public FruitController(FruitService fruitService) {
        this.fruitService = fruitService;
    }

    /**
     * FIXME: cambiar la entidad fruit por un dto FruitDTO
     *
     * @return
     */
    @GetMapping("/fruits")
    public ResponseEntity<List<Fruit>> getAllFruits() {
        return ResponseEntity.ok()
                .headers(HeaderUtil.createAlert("ok", "ok"))
                .body(fruitService.findAllFruits());
    }



    @PostMapping("/fruit")
    public ResponseEntity<Fruit> createFruit(@Valid @RequestBody Fruit fruit) {
        Fruit fruitSaved = fruitService.save(fruit);

        if (fruitSaved.getFruitId() != null) {
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createAlert("OK", "Ok"))
                    .body(fruitSaved);
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createAlert("Error al insertar", "404"))
                    .body(new Fruit());
        }
    }

    @PutMapping("/fruit/{id}")
    public ResponseEntity<Fruit> updateFruit(@Valid @RequestBody Fruit fruit, @PathVariable Long id) {

        Fruit fruitSaved = fruitService.update(fruit, id);

        if (fruitSaved.getFruitId() != null) {
            return ResponseEntity.ok()
                    .headers(HeaderUtil.createAlert("OK", "Ok"))
                    .body(fruitSaved);
        } else {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createAlert("Error al actualizar", "404"))
                    .body(new Fruit());
        }
    }
    @DeleteMapping("/fruit/{id}")
    public ResponseEntity<Void> deleteFruit(@PathVariable Long id) {

        fruitService.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(Fruit.class.getName(), id.toString())).build();
    }
}
