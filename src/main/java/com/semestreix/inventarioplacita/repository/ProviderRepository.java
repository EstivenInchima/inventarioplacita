package com.semestreix.inventarioplacita.repository;

import com.semestreix.inventarioplacita.domain.Cereal;
import com.semestreix.inventarioplacita.domain.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

import java.util.List;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Long> {

    /**
     *
     * buscar todo
     */
    List<Provider> findAll();

    /**
     * ordenar asc
     */
    List<Provider> findAllByOrderByProviderNameAsc();

    /**
     * buscar  por nombre
     */
    List<Provider> findAllByProviderName(@Param("providerName") String  Name);

    /**
     * buscar por nombre y empresa
     */

    List<Provider> findAllByProviderNameAndProviderNameBussiness(@Param("providerName") String Name, @Param("providerNameBussiness") String Bussiness);

    /**
     * numero de productos
     */
    long count();

    /**
     * cantidad de productos por nombre
     */

    long countAllByProviderName(@Param("providerName") String countName);


}
