package com.semestreix.inventarioplacita.repository;


import com.semestreix.inventarioplacita.domain.Legume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LegumeRepository extends JpaRepository<Legume, Long> {

    /**
     * buscar todo
     */
    List<Legume> findAllBy();

    /**
     * ordenar asc
     */
    List<Legume> findAllByOrderByLegumeNameAsc();

    /**
     * buscar  por nombre
     */
    List<Legume> findAllByLegumeName(@Param("legumeName") String  Name);

    /**
     * buscar por id y precio
     */

    List<Legume> findAllByLegumeIdAndLegumePrice(@Param("legumeId") Long Id, @Param("legumePrice") Long Price);

    /**
     * numero de productos
     */
    long count();

    /**
     * cantidad de productos por nombre
     */

    long countAllByLegumeName(@Param("legumeName") String countName);


}
