package com.semestreix.inventarioplacita.repository;

import com.semestreix.inventarioplacita.domain.Cereal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

import java.util.List;

@Repository
public interface CerealRepository extends JpaRepository<Cereal, Long> {

    /**
     *
     * buscar todo
     */
    List<Cereal> findAllBy();

    /**
     * ordenar asc
     */
    List<Cereal> findAllByOrderByCerealNameAsc();

    /**
     * buscar  por nombre
     */
     List<Cereal> findAllByCerealName(@Param("cerealName") String  Name);

    /**
     * buscar por id y precio
     */

    List<Cereal> findAllByCerealIdAndCerealPrice(@Param("cerealId") Long Id, @Param("cerealPrice") Long Price);

    /**
     * numero de productos
     */
    long count();

    /**
     * cantidad de productos por nombre
     */

    long countAllByCerealName(@Param("cerealName") String countName);


}
