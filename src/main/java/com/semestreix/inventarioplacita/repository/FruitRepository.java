package com.semestreix.inventarioplacita.repository;

import com.semestreix.inventarioplacita.domain.Fruit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FruitRepository  extends JpaRepository<Fruit, Long> {

    /**
     *
     * buscar todo
     */

    List<Fruit> findAll();

    /**
     * ordenar asc
     */
    List<Fruit> findAllByOrderByFruitNameAsc();

    /**
     * buscar  por nombre
     */
    List<Fruit> findAllByFruitName(@Param("fruitName") String name);

    /**
     * buscar por descripcion
     */
     List<Fruit> findAllByFruitDescription(@Param("fruitDescription")String Description);
    /**
     * buscar por id y precio
     */

    List<Fruit> findAllByFruitIdAndFruitPrice(@Param("fruitId") Long Id, @Param("fruitPrice") Long Price);

    /**
     * numero de productos
     */
    long count();

    /**
     * cantidad de productos por nombre
     */

    long countAllByFruitName(@Param("fruitName") String countName);

    /**
     * buscar un solo registro por id
     *
     * @return
     */

    Optional<Fruit> findByFruitId(@Param("id")Long id);



    //                                                                        LLLLLLLLLLLLLL
    //  @Transactional
    @Modifying
    @Query("update Fruit fr set fr.fruitName = :name where fr.fruitId = :id")
    int updateName(@Param("name") String name, @Param("id") Long id);


    @Modifying
    @Query("update Fruit fr set fr.fruitDescription = :description where fr.fruitId = :id")
    int updateDescription(@Param("description") String description, @Param("id") Long id);

    @Modifying
    @Query("update Fruit fr set fr.fruitPrice = :price where fr.fruitId = :id")
    int updatePrice(@Param("price") Long price, @Param("id") Long id);




}
