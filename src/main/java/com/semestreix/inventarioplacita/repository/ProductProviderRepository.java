package com.semestreix.inventarioplacita.repository;



import com.semestreix.inventarioplacita.domain.ProductProvider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

import java.util.List;

@Repository
public interface ProductProviderRepository extends JpaRepository<ProductProvider, Long>{


    /**
     *
     * buscar todo
     */
    List<ProviderRepository> findAllBy();


    /**
     * numero de productos
     */
    long count();



}
