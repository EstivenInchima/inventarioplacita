package com.semestreix.inventarioplacita.repository;


import com.semestreix.inventarioplacita.domain.Cereal;
import com.semestreix.inventarioplacita.domain.Vegetable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

import java.util.List;

@Repository
public interface VegetableRepository extends JpaRepository<Vegetable, Long> {


    /**
     *
     * buscar todo
     */
    List<Vegetable> findAllBy();

    /**
     * ordenar asc
     */
    List<Vegetable> findAllByOrderByVegetableNameAsc();

    /**
     * buscar  por nombre
     */
    List<Vegetable> findAllByVegetableName(@Param("vegetableName") String  Name);

    /**
     * buscar por id y precio
     */

    List<Vegetable> findAllByVegetableIdAndVegetablePrice(@Param("vegetableId") Long Id, @Param("vegetablePrice") Long Price);

    /**
     * numero de productos
     */
    long count();

    /**
     * cantidad de productos por nombre
     */

    long countAllByVegetableName(@Param("vegetableName") String countName);



}
