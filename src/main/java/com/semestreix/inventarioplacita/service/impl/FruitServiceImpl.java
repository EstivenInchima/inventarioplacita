package com.semestreix.inventarioplacita.service.impl;

import com.semestreix.inventarioplacita.domain.Fruit;
import com.semestreix.inventarioplacita.repository.FruitRepository;
import com.semestreix.inventarioplacita.service.FruitService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class FruitServiceImpl implements FruitService {

    private final FruitRepository fruitRepository;

    public FruitServiceImpl(FruitRepository fruitRepository) {
        this.fruitRepository = fruitRepository;
    }

    /**
     * cuerpo del microservicio
     *
     * @return
     */
    @Transactional
    public List<Fruit> findAllFruits() {
        return fruitRepository.findAll();
    }

    @Override
    public Fruit findFruitById(Long id) {
        Fruit fruit = fruitRepository.findByFruitId(id).orElse(new Fruit());
        return fruit;
    }

    @Override
    public int updateNameInFruit(String newName, Long id) {
        return fruitRepository.updateName(newName,id);
    }

    @Override
    public Fruit save(Fruit sVf) {
        return fruitRepository.save(sVf);
    }

    @Override
    public Fruit update(Fruit uPf, Long id) {
        Fruit pOld = findFruitById(id);// busca a la persona en base de datos

        if (pOld != null && pOld.getFruitId() != null){ // verifica que esa persona si existe
            uPf.setFruitId(pOld.getFruitId());//actualiza el id de los datos que llegan en objeto uPf
            return  fruitRepository.save(uPf); //ACTUALIZAR --> todos los campos incluido los cambios
        }else{
            return save(uPf); // crear la persona uPf
        }
    }

    @Override
    public void delete(Fruit dlTf) {
        fruitRepository.delete(dlTf);
    }

    @Override
    public void deleteById(Long id) {
        fruitRepository.deleteById(id);

    }


}




