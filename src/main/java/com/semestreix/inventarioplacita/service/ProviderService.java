package com.semestreix.inventarioplacita.service;

import com.semestreix.inventarioplacita.domain.Fruit;
import com.semestreix.inventarioplacita.domain.Provider;

import java.util.List;

public interface ProviderService {


    /**
     * micro-servicio que retorna todos los registros de la tabla tbl_fruit
     * @return
     */
    List<Provider> findAllProviders();

    /**
     * micro-servicio que retorn un regostro con la condicion where de
     * ser igual al id que se pase
     */

    Provider findProviderById(Long id);


    /**
     * microservicio que retorn si actulizo o no ,
     * la columna nombre de la tbl fruit
     */

    int updateNameInProvider(String newName, Long id);

    /**
     * microservicio retorna el objeto de tipo fruit que se persiste en la BD,
     * insert into tbl fruit ...
     */
    Provider save(Provider sVp);

    /**
     * micro-servicio que retorna el objeto de tipo fruit que representa la tbl fruit
     * y en este caso se actualiza todos los campos que sean diferentes a lo que este
     * en bd
     */

    Provider update(Provider uPp, Long id);

    /**
     * micro-servicio no retorna nada,
     * y se encarga de borrar un registro de la tabla fruit por objeto
     * @param dlTp
     */
    void delete(Provider dlTp);

    /** ----mejor opcion ----
     * micro-servicio no retorna nada,
     * y se encarga de borrar un registro de la tabla fruit por id
     */
    void deleteById(Long id);
}
