package com.semestreix.inventarioplacita.service;

import com.semestreix.inventarioplacita.domain.Fruit;

import java.util.List;

public interface FruitService {


    /**
     * micro-servicio que retorna todos los registros de la tabla tbl_fruit
     * @return
     */
    List<Fruit> findAllFruits();

    /**
     * micro-servicio que retorn un regostro con la condicion where de
     * ser igual al id que se pase
     */

     Fruit findFruitById(Long id);

    /**
     * microservicio que retorn si actulizo o no ,
     * la columna nombre de la tbl fruit
     */

    int updateNameInFruit(String newName, Long id);

    /**
     * microservicio retorna el objeto de tipo fruit que se persiste en la BD,
     * insert into tbl fruit ...
     */
     Fruit save(Fruit sVf);

    /**
     * micro-servicio que retorna el objeto de tipo fruit que representa la tbl fruit
     * y en este caso se actualiza todos los campos que sean diferentes a lo que este
     * en bd
     */

    Fruit update(Fruit uPf, Long id);

    /**
     * micro-servicio no retorna nada,
     * y se encarga de borrar un registro de la tabla fruit por objeto
     * @param dlTf
     */
    void delete(Fruit dlTf);

    /** ----mejor opcion ----
     * micro-servicio no retorna nada,
     * y se encarga de borrar un registro de la tabla fruit por id
     */
    void deleteById(Long id);

//
//
//    /**
//     * micro-servicio que retorna un registro con la condicion where de ser igual al id que se pase
//     *
//     * @param id
//     * @return
//     */
//    Fruit findFruitByFruit(Long id);
//
//    /**
//     * micro-servicio que retorna si actualizo o no, la columna nombre de la tbl persona, por id
//     *
//     * @param newName
//     * @param id
//     * @return
//     */
  //int updateNameInFruit(String newName, Long id);

}
