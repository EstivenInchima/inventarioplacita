package com.semestreix.inventarioplacita.domain.dto;



public class FruitDTO {

    private Long fruitId;
    private String fruitName;
    private Long fruitPrice;
    private String fruitDescription;

    public Long getFruitId() {
        return fruitId;
    }

    public void setFruitId(Long fruitId) {
        this.fruitId = fruitId;
    }

    public String getFruitName() {
        return fruitName;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

    public Long getFruitPrice() {
        return fruitPrice;
    }

    public void setFruitPrice(Long fruitPrice) {
        this.fruitPrice = fruitPrice;
    }

    public String getFruitDescription() {
        return fruitDescription;
    }

    public void setFruitDescription(String fruitDescription) {
        this.fruitDescription = fruitDescription;
    }
}
