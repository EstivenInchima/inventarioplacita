package com.semestreix.inventarioplacita.domain.dto;

public class VegetableDTO {

    private Long vegetableId;
    private String vegetableName;
    private Long vegetablePrice;
    private String vegetableDescription;


    public Long getVegetableId() {
        return vegetableId;
    }

    public void setVegetableId(Long vegetableId) {
        this.vegetableId = vegetableId;
    }

    public String getVegetableName() {
        return vegetableName;
    }

    public void setVegetableName(String vegetableName) {
        this.vegetableName = vegetableName;
    }

    public Long getVegetablePrice() {
        return vegetablePrice;
    }

    public void setVegetablePrice(Long vegetablePrice) {
        this.vegetablePrice = vegetablePrice;
    }

    public String getVegetableDescription() {
        return vegetableDescription;
    }

    public void setVegetableDescription(String vegetableDescription) {
        this.vegetableDescription = vegetableDescription;
    }
}
