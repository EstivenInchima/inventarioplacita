package com.semestreix.inventarioplacita.domain.dto;

public class CerealDTO {
    private Long cerealId;
    private String cerealName;
    private Long cerealPrice;
    private String cerealDescription;

    public Long getCerealId() {
        return cerealId;
    }

    public void setCerealId(Long cerealId) {
        this.cerealId = cerealId;
    }

    public String getCerealName() {
        return cerealName;
    }

    public void setCerealName(String cerealName) {
        this.cerealName = cerealName;
    }

    public Long getCerealPrice() {
        return cerealPrice;
    }

    public void setCerealPrice(Long cerealPrice) {
        this.cerealPrice = cerealPrice;
    }

    public String getCerealDescription() {
        return cerealDescription;
    }

    public void setCerealDescription(String cerealDescription) {
        this.cerealDescription = cerealDescription;
    }
}
