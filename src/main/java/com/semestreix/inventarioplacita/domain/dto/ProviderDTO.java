package com.semestreix.inventarioplacita.domain.dto;

public class ProviderDTO {

    private Long providerId;
    private String providerName;
    private String providerLastName;
    private Long providerIdentification;
    private Long providerTelephone;
    private String providerNameBussiness;

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderLastName() {
        return providerLastName;
    }

    public void setProviderLastName(String providerLastName) {
        this.providerLastName = providerLastName;
    }

    public Long getProviderIdentification() {
        return providerIdentification;
    }

    public void setProviderIdentification(Long providerIdentification) {
        this.providerIdentification = providerIdentification;
    }

    public Long getProviderTelephone() {
        return providerTelephone;
    }

    public void setProviderTelephone(Long providerTelephone) {
        this.providerTelephone = providerTelephone;
    }

    public String getProviderNameBussiness() {
        return providerNameBussiness;
    }

    public void setProviderNameBussiness(String providerNameBussiness) {
        this.providerNameBussiness = providerNameBussiness;
    }
}
