package com.semestreix.inventarioplacita.domain.dto;

public class LegumeDTO {
    private Long legumeId;
    private String legumeName;
    private Long legumePrice;
    private String legumeDescription;

    public Long getLegumeId() {
        return legumeId;
    }

    public void setLegumeId(Long legumeId) {
        this.legumeId = legumeId;
    }

    public String getLegumeName() {
        return legumeName;
    }

    public void setLegumeName(String legumeName) {
        this.legumeName = legumeName;
    }

    public Long getLegumePrice() {
        return legumePrice;
    }

    public void setLegumePrice(Long legumePrice) {
        this.legumePrice = legumePrice;
    }

    public String getLegumeDescription() {
        return legumeDescription;
    }

    public void setLegumeDescription(String legumeDescription) {
        this.legumeDescription = legumeDescription;
    }
}
