package com.semestreix.inventarioplacita.domain;

import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
@Entity
@Table(
        name = "tbl_legume"
)



public class Legume implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "legume_id", nullable = false)
    private Long legumeId;

    @Column(name = "legume_name", nullable = false)
    @NotNull
    private String legumeName;

    @Column(name = "legume_price", nullable = false)
    @NotNull
    private Long legumePrice;

    @Size(max = 100)
    @Column(name = "legume_description", nullable = false)
    private String legumeDescription;

    public Long getLegumeId() {
        return legumeId;
    }

    public void setLegumeId(Long legumeId) {
        this.legumeId = legumeId;
    }

    public String getLegumeName() {
        return legumeName;
    }

    public void setLegumeName(String legumeName) {
        this.legumeName = legumeName;
    }

    public Long getLegumePrice() {
        return legumePrice;
    }

    public void setLegumePrice(Long legumePrice) {
        this.legumePrice = legumePrice;
    }

    public String getLegumeDescription() {
        return legumeDescription;
    }

    public void setLegumeDescription(String legumeDescription) {
        this.legumeDescription = legumeDescription;
    }
}
