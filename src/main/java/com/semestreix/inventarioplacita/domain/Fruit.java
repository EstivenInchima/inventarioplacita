package com.semestreix.inventarioplacita.domain;



import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "tbl_fruit")
public class Fruit implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "fruit_id", nullable = false)
    private Long fruitId;

    @Column(name = "fruit_name", nullable = false)
    private String fruitName;


    @Column(name = "fruit_price", nullable = false)
    private Long fruitPrice;

    //@Size(max = 100)
    @Column(name = "fruit_description", nullable = false)
    private String fruitDescription;

    public Long getFruitId() {
        return fruitId;
    }

    public void setFruitId(Long fruitId) {
        this.fruitId = fruitId;
    }

    public String getFruitName() {
        return fruitName;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

    public Long getFruitPrice() {
        return fruitPrice;
    }

    public void setFruitPrice(Long fruitPrice) {
        this.fruitPrice = fruitPrice;
    }

    public String getFruitDescription() {
        return fruitDescription;
    }

    public void setFruitDescription(String fruitDescription) {
        this.fruitDescription = fruitDescription;
    }
}
