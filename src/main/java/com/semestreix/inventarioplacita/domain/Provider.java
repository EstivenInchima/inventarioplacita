package com.semestreix.inventarioplacita.domain;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(
        name = "tbl_provider"
)

public class Provider implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "provider_id", nullable = false)
    private Long providerId;

    @Column (name = "provider_name", nullable = false)

    private String providerName;

    @Column(name = "provider_lastname", nullable = false)

    private String providerLastName;

    @Column(name = "provider_identification", nullable = false ,unique = true)
    @NotNull
    private Long providerIdentification;

    @Column(name = "provider_telephone", nullable = false)
    @NotNull
    private Long providerTelephone;

    @Column(name = "provider_namebussiness", nullable = false)
    private String providerNameBussiness;

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderLastName() {
        return providerLastName;
    }

    public void setProviderLastName(String providerLastName) {
        this.providerLastName = providerLastName;
    }

    public Long getProviderIdentification() {
        return providerIdentification;
    }

    public void setProviderIdentification(Long providerIdentification) {
        this.providerIdentification = providerIdentification;
    }

    public Long getProviderTelephone() {
        return providerTelephone;
    }

    public void setProviderTelephone(Long providerTelephone) {
        this.providerTelephone = providerTelephone;
    }

    public String getProviderNameBussiness() {
        return providerNameBussiness;
    }

    public void setProviderNameBussiness(String providerNameBussiness) {
        this.providerNameBussiness = providerNameBussiness;
    }
}

