package com.semestreix.inventarioplacita.domain;

import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

@Entity
@Table(
        name = "tbl_cereal"
)


public class Cereal implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cereal_id", nullable = false)
    private Long cerealId;

    @Column(name = "cereal_name", nullable = false)
    @NotNull
    private String cerealName;

    @Column(name = "cereal_price", nullable = false)
    @NotNull
    private Long cerealPrice;

    @Size(max = 100)
    @Column(name = "cereal_description", nullable = false)
    private String cerealDescription;

    public Long getCerealId() {
        return cerealId;
    }

    public void setCerealId(Long cerealId) {
        this.cerealId = cerealId;
    }

    public String getCerealName() {
        return cerealName;
    }

    public void setCerealName(String cerealName) {
        this.cerealName = cerealName;
    }

    public Long getCerealPrice() {
        return cerealPrice;
    }

    public void setCerealPrice(Long cerealPrice) {
        this.cerealPrice = cerealPrice;
    }

    public String getCerealDescription() {
        return cerealDescription;
    }

    public void setCerealDescription(String cerealDescription) {
        this.cerealDescription = cerealDescription;
    }
}
