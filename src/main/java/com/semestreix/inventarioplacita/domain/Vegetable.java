package com.semestreix.inventarioplacita.domain;

import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
@Entity
@Table(
        name = "tbl_vegetable"
)


public class Vegetable implements Serializable {

    @Id

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "vegetable_id", nullable = false)
    private Long vegetableId;

    @Column(name = "vegetable_name", nullable = false)
    @NotNull
    private String vegetableName;

    @Column(name = "vegetable_price", nullable = false)
    @NotNull
    private Long vegetablePrice;

    @Size(max = 100)
    @Column(name = "vegetable_description", nullable = false)
    private String vegetableDescription;

    public Long getVegetableId() {
        return vegetableId;
    }

    public void setVegetableId(Long vegetableId) {
        this.vegetableId = vegetableId;
    }

    public String getVegetableName() {
        return vegetableName;
    }

    public void setVegetableName(String vegetableName) {
        this.vegetableName = vegetableName;
    }

    public Long getVegetablePrice() {
        return vegetablePrice;
    }

    public void setVegetablePrice(Long vegetablePrice) {
        this.vegetablePrice = vegetablePrice;
    }

    public String getVegetableDescription() {
        return vegetableDescription;
    }

    public void setVegetableDescription(String vegetableDescription) {
        this.vegetableDescription = vegetableDescription;
    }
}
