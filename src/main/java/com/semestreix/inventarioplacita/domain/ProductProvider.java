package com.semestreix.inventarioplacita.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Entity
@Table(
        name = "tbl_product_provider"
)

public class ProductProvider implements Serializable {

     private static final long serialVersionUID = 1L;

     @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "product_provider_id", nullable = false)
    private Long product_provider_id;

    @NotNull
    @Column(name = "provider_id", nullable = false)
    private Long providerId;

    @JoinColumn(name = "provider_id", referencedColumnName = "provider_id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Provider provider;


    @Column(name = "cereal_id", nullable = false)
    private Long cerealId;

    @JoinColumn(name = "cereal_id", referencedColumnName = "cereal_id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Cereal cereal;

    @Column(name = "legume_id", nullable = false)
    private Long legumeId;

    @JoinColumn(name = "legume_id", referencedColumnName = "legume_id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Legume legume;

    @Column(name = "vegetable_id", nullable = false)
    private Long vegetableId;

    @JoinColumn(name = "vegetable_id", referencedColumnName = "vegetable_id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Vegetable vegetable;


    @Column(name = "fruit_id", nullable = false)
    private Long fruitId;

    @JoinColumn(name = "fruit_id", referencedColumnName = "fruit_id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Fruit fruit;




}
