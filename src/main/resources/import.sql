insert into tbl_provider(provider_id, provider_name, provider_lastname, provider_identification, provider_telephone,provider_namebussiness) values(1,'felipe', 'tercero', 1234, 4321, 'colombina');
insert into tbl_provider(provider_id, provider_name, provider_lastname, provider_identification, provider_telephone,provider_namebussiness) values(2, 'pepito', 'segundo', 2341, 2134, 'alqueria');
insert into tbl_provider(provider_id, provider_name, provider_lastname, provider_identification, provider_telephone,provider_namebussiness) values(3,'jesus', 'primero', 3412, 3214, 'alpina');

insert into tbl_legume(legume_id, legume_name, legume_price, legume_description ) values(11,'frijol', 10000, 'rojo');
insert into tbl_legume(legume_id, legume_name, legume_price, legume_description )values(22,'habichuela', 12000, 'verde');
insert into tbl_legume(legume_id, legume_name, legume_price, legume_description) values(33,'garbanzo',8000, 'amarillo');

insert into tbl_cereal(cereal_id,cereal_name, cereal_price, cereal_description) values(01,'maiz', 10000, 'amarillo');
insert into tbl_cereal(cereal_id,cereal_name, cereal_price, cereal_description) values(02,'arroz', 10000, 'blanco');
insert into tbl_cereal(cereal_id,cereal_name, cereal_price, cereal_description) values(03,'trigo', 10000, 'cafe');

insert into tbl_fruit(fruit_id,fruit_name, fruit_price, fruit_description) values(001,'manzana', 900, 'venta por unidad ,roja');
insert into tbl_fruit(fruit_id,fruit_name, fruit_price, fruit_description) values(002,'pera', 700, 'venta por unidad ,verde');
insert into tbl_fruit(fruit_id,fruit_name, fruit_price, fruit_description) values(003,'aguacate', 500, 'venta por unidad ,verde');

insert into tbl_vegetable (vegetable_id,vegetable_name, vegetable_price, vegetable_description) values(111,'espinaca', 500, 'monton,verde');
insert into tbl_vegetable (vegetable_id,vegetable_name, vegetable_price, vegetable_description) values(222,'cebolla', 500, 'peso por unidad(kg),blanca');
insert into tbl_vegetable (vegetable_id,vegetable_name, vegetable_price, vegetable_description) values(333,'tomate de mesa', 500, 'unidad,rojo');

insert into tbl_product_provider(provider_id,cereal_id,legume_id,vegetable_id,fruit_id) values('1','03','33','111','002');
insert into tbl_product_provider(provider_id,cereal_id,legume_id,vegetable_id,fruit_id) values('1','02','11','222','001');
insert into tbl_product_provider(provider_id,cereal_id,legume_id,vegetable_id,fruit_id) values('1','01','22','333','003');